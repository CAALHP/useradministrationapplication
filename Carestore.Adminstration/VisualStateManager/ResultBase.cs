﻿using System;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace Carestore.Adminstration.VisualStateManager
{
    public abstract class ResultBase : IResult
    {
        public event EventHandler<ResultCompletionEventArgs> Completed = delegate { };

        protected virtual void OnCompleted()
        {
            OnCompleted(new ResultCompletionEventArgs());
        }

        protected virtual void OnError(Exception error)
        {
            OnCompleted(new ResultCompletionEventArgs
            {
                Error = error
            });
        }

        protected virtual void OnCompleted(ResultCompletionEventArgs e)
        {
            Caliburn.Micro.Execute.OnUIThread(() => Completed(this, e));
        }

        public abstract Task ExecuteAsync<T>(T context);

        public abstract void Execute(CoroutineExecutionContext context);
    }
}
