﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Caliburn.Micro;

namespace Carestore.Adminstration.VisualStateManager
{
    public class VisualStateResult : ResultBase
    {
        private readonly string stateName;
        private readonly bool useTransitions;

        public VisualStateResult(string stateName, bool useTransitions = true)
        {
            this.stateName = stateName;
            this.useTransitions = useTransitions;
        }

        public string StateName
        {
            get { return stateName; }
        }

        public bool UseTransitions
        {
            get { return useTransitions; }
        }

        public async override Task ExecuteAsync<T>(T context)
        {
            if (!(context is CoroutineExecutionContext))
            {
                await Caliburn.Micro.Execute.OnUIThreadAsync(() =>
                {
                    try
                    {
                            var view = ViewLocator.LocateForModel(context, null, null) as Control;
                            Execute(new CoroutineExecutionContext { View = view });
                    }
                    catch { }
                });
            }
            else
                Execute(context as CoroutineExecutionContext);
        }

        public override void Execute(CoroutineExecutionContext context)
        {
            if (!(context.View is Control))
                throw new InvalidOperationException("View must be a Control to use VisualStateResult");

            var view = (Control)context.View;

            var result = System.Windows.VisualStateManager.GoToState(view, StateName, UseTransitions);

            if (!result)
                throw new Exception("Go to state failed");

            OnCompleted();
        }
    }
}
