﻿using System;
using System.Globalization;
using System.Windows;

namespace Carestore.Adminstration
{
    public partial class AppRoot : Application
    {
        private static AppRoot _app;
        private readonly AdminstrationBootstrapper _bootstrapper;

        public AppRoot()
        {
            // http://stackoverflow.com/questions/520115/stringformat-localization-problem/520334#520334
            FrameworkElement.LanguageProperty.OverrideMetadata(
                typeof(FrameworkElement),
                new FrameworkPropertyMetadata(
                    System.Windows.Markup.XmlLanguage.GetLanguage(
                        System.Threading.Thread.CurrentThread.CurrentUICulture.IetfLanguageTag
                    )
                )
            );

            Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary { Source = new Uri("pack://application:,,,/Carestore.Adminstration;component/Resources/Colors.xaml") });

            _bootstrapper = new AdminstrationBootstrapper();
            this.DispatcherUnhandledException += AppRoot_DispatcherUnhandledException;
        }

        void AppRoot_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
        }

        protected override void OnExit(ExitEventArgs e)
        {
            _bootstrapper.Dispose();
            base.OnExit(e);
        }

        [System.STAThreadAttribute()]
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.LoaderOptimization(LoaderOptimization.MultiDomainHost)]
        public static void Main()
        {
            SetThreadCultures(); // must be set first
            _app = new AppRoot();
            _app.Run();
        }

        private static void SetThreadCultures()
        {
            // Set threading culture
            //System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("de-DE");
            System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.CurrentCulture;
            CultureInfo.DefaultThreadCurrentCulture = System.Threading.Thread.CurrentThread.CurrentUICulture;
            CultureInfo.DefaultThreadCurrentUICulture = System.Threading.Thread.CurrentThread.CurrentUICulture;
        }
    }
}
