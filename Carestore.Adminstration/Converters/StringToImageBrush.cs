﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Carestore.Adminstration.Converters
{
    [ValueConversion(typeof(object), typeof(string))]  
    public class StringToImageBrush : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ImageSource bitMap = string.IsNullOrEmpty((string)value) ? new BitmapImage(new Uri("pack://application:,,,/Carestore.Adminstration;component/Images/DefaultProfilePicture.jpg")) : new BitmapImage(new Uri((string)value));

            return new ImageBrush().ImageSource = bitMap;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
