﻿using System.Windows;
using Carestore.Adminstration.Interfaces;
using Carestore.Adminstration.VisualStateManager;
using FluidKit.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carestore.Adminstration.ViewModels
{
    public class ShellViewModel : Caliburn.Micro.Conductor<object>.Collection.OneActive, Interfaces.IShell, Caliburn.Micro.IHandleWithTask<Messages.RequestBiometricsView>, Caliburn.Micro.IHandleWithTask<Messages.RequestCreateUserView>, Caliburn.Micro.IHandleWithTask<Messages.RequestLoginView>
    {
        private TransitionPresenter _transContainer;
        private string _currentView;

        public ViewModels.LoginViewModel LoginViewModel { get; private set; }
        public ViewModels.CreateUserViewModel CreateUserViewModel { get; private set; }
        public ViewModels.BiometricsViewModel BiometricsViewModel { get; private set; }

        private bool _canLogOut;
        public bool CanLogOut
        {
            get { return _canLogOut; }
            set
            {
                _canLogOut = value;
                NotifyOfPropertyChange(() => CanLogOut);
            }
        }

        public ShellViewModel(ViewModels.LoginViewModel loginViewModel, ViewModels.CreateUserViewModel createUserViewModel, ViewModels.BiometricsViewModel biometricsViewModel) 
        {
            if (loginViewModel == null)
                throw new ArgumentNullException("loginViewModel");
            if (createUserViewModel == null)
                throw new ArgumentNullException("createUserViewModel");
            if (biometricsViewModel == null)
                throw new ArgumentNullException("biometricsViewModel");

            LoginViewModel = loginViewModel;
            CreateUserViewModel = createUserViewModel;
            BiometricsViewModel = biometricsViewModel;
        }

        protected async override void OnViewReady(object view)
        {
            base.OnViewReady(view);

            var shellView = view as Views.ShellView;
            if (shellView == null)
                return;

            _transContainer = shellView._transContainer;
            _transContainer.ApplyTransition("CreateUserViewModel", "LoginViewModel");
            _currentView = "LoginViewModel";
            CanLogOut = false;
        }

        public void Shutdown()
        {
            Application.Current.Shutdown();
        }

        public void Minimize()
        {
            Application.Current.MainWindow.WindowState = WindowState.Minimized;            
        }

        public async Task Handle(Messages.RequestBiometricsView message)
        {
            Activate(BiometricsViewModel);
            ChangeView("BiometricsViewModel");
            CanLogOut = true;
        }
        
        public async Task Handle(Messages.RequestLoginView message)
        {
            Activate(LoginViewModel);
            ChangeView("LoginViewModel");
            CanLogOut = false;
        }

        public void LogOut()
        {
            Activate(LoginViewModel);
            ChangeView("LoginViewModel");
            CanLogOut = false;
        }

        public async Task Handle(Messages.RequestCreateUserView message)
        {
            Activate(CreateUserViewModel);
            ChangeView("CreateUserViewModel"); 
            CanLogOut = true;
        }

        private void ChangeView(string view)
        {
            Caliburn.Micro.Execute.OnUIThread(() => 
            {
                _transContainer.ApplyTransition(_currentView, view);
                Deactivate();
                _currentView = view;
            });
        }

        private void Deactivate()
        {
            switch (_currentView)
            {
                case "CreateUserViewModel":
                    DeactivateItem(CreateUserViewModel, false);
                    break;
                case "LoginViewModel":
                    DeactivateItem(LoginViewModel, false);
                    break;
                case "BiometricsViewModel":
                    DeactivateItem(BiometricsViewModel, false);
                    break;
            }
        }

        private void Activate(object viewModel)
        {
            var window = viewModel as Caliburn.Micro.IActivate;
            if (window != null)
                window.Activate();
        }
    }
}
