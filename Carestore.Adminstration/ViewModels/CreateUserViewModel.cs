﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Carestore.Adminstration.Interfaces;
using Carestore.Adminstration.Messages;
using Carestore.Adminstration.Models;
using Carestore.Adminstration.VisualStateManager;

namespace Carestore.Adminstration.ViewModels
{
    public class CreateUserViewModel : Caliburn.Micro.Screen
    {
        private readonly Caliburn.Micro.IEventAggregator _events;
        private readonly Interfaces.IMarketplace _marketplace;
        private readonly Interfaces.IAuthenticationManager _authenticationManager;
        private IReadOnlyCollection<User> _staticUsers; 

        private IReadOnlyCollection<User> _users;
        public IReadOnlyCollection<User> Users
        {
            get { return _users; }
            set
            {
                _users = value;
                NotifyOfPropertyChange(()=> Users);
            }
        }
        
        public CreateUserViewModel(Caliburn.Micro.IEventAggregator events, IMarketplace marketplace, IAuthenticationManager authenticationManager)
        {
            if (events == null)
                throw new ArgumentNullException("events");
            if (marketplace == null)
                throw new ArgumentNullException("marketplace");
            if (authenticationManager == null)
                throw new ArgumentNullException("authenticationManager");

            _events = events;
            _marketplace = marketplace;
            _authenticationManager = authenticationManager;
        }

        protected async override void OnActivate()
        {
            await new VisualStateResult("LoadingUsers").ExecuteAsync(this);
            if (_authenticationManager.GetAuthUser() != string.Empty)
            {
                _staticUsers = await _marketplace.GetUsersAsync(_authenticationManager.GetAuthUser());
                Users = new ReadOnlyCollection<User>(_staticUsers.ToList());
            }
            else
                _events.Publish(new RequestLoginView(), action => Task.Factory.StartNew(action));
            await new VisualStateResult("Default").ExecuteAsync(this);
        }

        public async Task UpdateUsers()
        {
            await new VisualStateResult("LoadingUsers").ExecuteAsync(this);
            if (_authenticationManager.GetAuthUser() != string.Empty)
            {
                _staticUsers = await _marketplace.GetUsersAsync(_authenticationManager.GetAuthUser());
                Users = new ReadOnlyCollection<User>(_staticUsers.ToList());
            }
            else
                _events.Publish(new RequestLoginView(), action => Task.Factory.StartNew(action));
            await new VisualStateResult("Default").ExecuteAsync(this);
        }

        public void SearchUsers(string locationSearchString)
        {
            if (!string.IsNullOrEmpty(locationSearchString) && !string.IsNullOrWhiteSpace(locationSearchString))
                Users = GetFilteredChildren(locationSearchString, _staticUsers);
            else
                Users = _staticUsers;
        }

        public void ChooseProfile(User user)
        {
            _events.Publish(new Messages.RequestBiometricsView(), action => Task.Factory.StartNew(action));
            _events.Publish(new Messages.NewUserEvent{User = user}, action => Task.Factory.StartNew(action));
        }

        private IReadOnlyCollection<User> GetFilteredChildren(string locationSearchString, IReadOnlyCollection<User> staticUsers)
        {
            return staticUsers.Where(loc => SearchString(loc.Firstname.ToLower() + " " + loc.Lastname, locationSearchString.ToLower())).ToList();
        }

        private bool SearchString(string value, string stringToSearch)
        {
            var keyWords = value.ToLower().Split(' ');
            var keyWordsToSearch = stringToSearch.ToLower().Split(' ');

            return (from keyWord in keyWords from s in keyWordsToSearch.Where(s => !string.IsNullOrEmpty(s) && !string.IsNullOrWhiteSpace(s)) select keyWord.Contains(s)).Any(result => result);
        }
    }
}
