﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Carestore.Adminstration.Interfaces;
using Carestore.Adminstration.Models;

namespace Carestore.Adminstration.ViewModels
{
    public class LoginViewModel : Caliburn.Micro.Screen
    {
        private readonly Caliburn.Micro.IEventAggregator _events;
        private readonly IAuthenticationManager _authenticationManager;

        public LoginViewModel(Caliburn.Micro.IEventAggregator events, IAuthenticationManager authenticationManager)
        {
            if (authenticationManager == null)
                throw new ArgumentNullException("authenticationManager");
            if (events == null)
                throw new ArgumentNullException("events");

            _authenticationManager = authenticationManager;
            _events = events;
        }

        public async Task Login(string username, string password)
        {
            await new VisualStateManager.VisualStateResult("Loading").ExecuteAsync(this);
            var isAuth = await _authenticationManager.LoginUser(new UserCredentialsData{UserLogin = username, UserPassword = password});

            if(isAuth)
            {
                await new VisualStateManager.VisualStateResult("LoginSucced").ExecuteAsync(this);
                await Task.Delay(400);
                _events.Publish(new Messages.RequestCreateUserView(), action => Task.Factory.StartNew(action));
                await new VisualStateManager.VisualStateResult("DefaultLoading").ExecuteAsync(this);
                return;
            }

            await new VisualStateManager.VisualStateResult("DefaultLoading").ExecuteAsync(this);            
            await new VisualStateManager.VisualStateResult("LoginFailed").ExecuteAsync(this);
        }
    }
}
