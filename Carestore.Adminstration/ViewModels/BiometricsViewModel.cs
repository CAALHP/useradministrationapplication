﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Carestore.Adminstration.Models;

namespace Carestore.Adminstration.ViewModels
{
    public class BiometricsViewModel : Caliburn.Micro.Screen, Caliburn.Micro.IHandleWithTask<Messages.NewUserEvent>
    {
        private readonly Interfaces.ICRIP _crip;
        private readonly Interfaces.IMarketplace _marketPlace;
        private readonly Caliburn.Micro.IEventAggregator _events;

        private User _user;
        public User User
        {
            get { return _user; }
            private set
            {
                _user = value;
                NotifyOfPropertyChange(() => User);
            }
        }

        public BiometricsViewModel(Interfaces.ICRIP crip, Interfaces.IMarketplace marketPlace, Caliburn.Micro.IEventAggregator events)
        {
            if (crip == null)
                throw new ArgumentNullException("crip");
            if (marketPlace == null)
                throw new ArgumentNullException("marketPlace");
            if (events == null)
                throw new ArgumentNullException("events");

            _crip = crip;
            _marketPlace = marketPlace;
            _events = events;
        }

        public async Task StoreBiometric()
        {
            await new VisualStateManager.VisualStateResult("StoreBio").ExecuteAsync(this);
            User.Biometric = await _crip.GetBiometrics();

            if (_user.Biometric != null && _user.Biometric.Any())
            {
                await new VisualStateManager.VisualStateResult("StoreUser").ExecuteAsync(this);
                var isStored = await _marketPlace.UpdateUserAsync(User);

                if (isStored)
                    await new VisualStateManager.VisualStateResult("UserStored").ExecuteAsync(this);
                else
                    await new VisualStateManager.VisualStateResult("FailedToStoreUser").ExecuteAsync(this);
            }
            else
                await new VisualStateManager.VisualStateResult("FailedToGetBio").ExecuteAsync(this);

            await Task.Delay(2000);
            await new VisualStateManager.VisualStateResult("Default").ExecuteAsync(this);
        }

        public async Task DeleteBio()
        {
            User.Biometric = new List<string>();
            await new VisualStateManager.VisualStateResult("StoreUser").ExecuteAsync(this);
            var isStored = await _marketPlace.UpdateUserAsync(User);

            if (isStored)
                await new VisualStateManager.VisualStateResult("UserStored").ExecuteAsync(this);
            else
                await new VisualStateManager.VisualStateResult("FailedToStoreUser").ExecuteAsync(this);

            await Task.Delay(2000);
            await new VisualStateManager.VisualStateResult("Default").ExecuteAsync(this);
        }

        public async Task StoreNFC()
        {
            await new VisualStateManager.VisualStateResult("StoreNFC").ExecuteAsync(this);
            var result = await _crip.StoreUserOnNFCCard(User.UserId);
            if(result)
                await new VisualStateManager.VisualStateResult("NFCStored").ExecuteAsync(this);
            else
                await new VisualStateManager.VisualStateResult("FailedToStoreNFC").ExecuteAsync(this);

            await Task.Delay(2000);
            await new VisualStateManager.VisualStateResult("Default").ExecuteAsync(this);
        }

        public void Back()
        {
            _events.Publish(new Messages.RequestCreateUserView(), action => Task.Factory.StartNew(action));
        }

        public async Task Handle(Messages.NewUserEvent message)
        {
            User = message.User;
        }
    }
}
