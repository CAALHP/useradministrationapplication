﻿using System;
using System.Threading.Tasks;
using Carestore.Adminstration.Interfaces;
using Carestore.Adminstration.Models;

namespace Carestore.Adminstration.Managers
{
    public class AuthenticationManager : IAuthenticationManager
    {
        private readonly IMarketplace _marketplace;
        private string _user;

        public AuthenticationManager(IMarketplace marketplace)
        {
            if (marketplace == null) 
                throw new ArgumentNullException("marketplace");

            _marketplace = marketplace;
        }

        public async Task<bool> LoginUser(UserCredentialsData userCredentials)
        {
            if (userCredentials == null)
                return false;

            _user = await _marketplace.VerifyLoginAsync(userCredentials);

            if (_user == "0")
                return false;
            
            return true;
        }

        public string GetAuthUser()
        {
            if (string.IsNullOrEmpty(_user) || _user == "0" || TimeSpan.FromSeconds(LastInputManager.GetSecondsSinceLastInput()) > TimeSpan.FromMinutes(15))
                return string.Empty;

            return _user;
        }
    }
}
