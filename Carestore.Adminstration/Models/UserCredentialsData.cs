﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carestore.Adminstration.Models
{
    public class UserCredentialsData // User Credential for login/password Sign In  
    {
        public string UserLogin { get; set; }         
        public string UserPassword { get; set; }
    }
}
