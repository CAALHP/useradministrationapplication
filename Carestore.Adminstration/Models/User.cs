﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carestore.Adminstration.Models
{
    public class User : Caliburn.Micro.PropertyChangedBase
    {
        private string _firstname;
        public string Firstname
        {
            get { return _firstname; }
            set 
            {
                _firstname = value;
                NotifyOfPropertyChange(() => Firstname);
            }
        }

        private string _lastname;

        public string Lastname
        {
            get { return _lastname; }
            set 
            {
                _lastname = value;
                NotifyOfPropertyChange(()=> Lastname);
            }
        }

        public int Role { get; set; }

        public string PictureURL { get; set; }

        private IReadOnlyCollection<string> _biometric;
        public IReadOnlyCollection<string> Biometric
        {
            get { return _biometric; }
            set
            {
                _biometric = value;
                NotifyOfPropertyChange(() => Biometric);
            }
        }

        public string UserId { get; set; }

        private string _nfc;
        public string NFC
        {
            get { return _nfc; }
            set
            {
                _nfc = value;
                NotifyOfPropertyChange(() => NFC);
            }
        }

        public override bool Equals(object obj)
        {
            var item = obj as User;

            if(item == null)
                return false;

            if (item.UserId == this.UserId)
                return true;

            return false;
        }
    }
}
