﻿namespace Carestore.Adminstration.Interfaces
{
    public interface IConfiguration
    {
        string CRIPAdress { get; }
        string MarketPlaceAdress { get; } 
    }
}