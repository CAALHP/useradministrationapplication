﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Carestore.Adminstration.Models;

namespace Carestore.Adminstration.Interfaces
{
    public interface IMarketplace
    {
        Task<string> VerifyLoginAsync(UserCredentialsData userCredentials);
        Task<bool> UpdateUserAsync(User user);
        Task<IReadOnlyCollection<Models.User>> GetUsersAsync(string managerId);
    }
}
