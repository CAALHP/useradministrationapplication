﻿using System.Threading.Tasks;
using Carestore.Adminstration.Models;

namespace Carestore.Adminstration.Interfaces
{
    public interface IAuthenticationManager
    {
        Task<bool> LoginUser(UserCredentialsData userCredentials);
        string GetAuthUser();
    }
}