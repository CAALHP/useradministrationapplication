﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carestore.Adminstration.Interfaces
{
    public interface ICRIP
    {
        Task<IReadOnlyCollection<string>> GetBiometrics();
        Task<bool> StoreUserOnNFCCard(string uID);
    }
}
