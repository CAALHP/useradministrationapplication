﻿using System.Linq;
using System.Windows;
using System;
using Caliburn.Micro;
using Autofac;
using System.Diagnostics;

namespace Carestore.Adminstration
{
    public class AdminstrationBootstrapper : Autofac.AutofacBootstrapper<Interfaces.IShell>
    {
        public AdminstrationBootstrapper()
        {
            Initialize();
        }

        protected override void ConfigureBootstrapper()
        {
            base.ConfigureBootstrapper();

            EnforceNamespaceConvention = true;
            AutoSubscribeEventAggegatorHandlers = true;
            ViewModelBaseType = typeof(IScreen);
            
            var p = Process.GetCurrentProcess();
            p.PriorityBoostEnabled = true;
            p.PriorityClass = ProcessPriorityClass.High;

            base.CreateEventAggregator = ()=> new EventAggregator();
        }

        protected override void ConfigureContainer(global::Autofac.ContainerBuilder builder)
        {
 	         base.ConfigureContainer(builder);

             builder.RegisterType<ViewModels.ShellViewModel>().AsImplementedInterfaces().SingleInstance();
             builder.RegisterType<ViewModels.BiometricsViewModel>().SingleInstance();
             builder.RegisterType<ViewModels.CreateUserViewModel>().SingleInstance();
             builder.RegisterType<ViewModels.LoginViewModel>().SingleInstance();

             builder.RegisterType<Managers.AuthenticationManager>().AsImplementedInterfaces().SingleInstance();

            builder.RegisterType<Configuration>().AsImplementedInterfaces().SingleInstance();

             //builder.RegisterType<Mocks.CripMock>().AsImplementedInterfaces().SingleInstance();
             builder.RegisterType<Repositories.CRIP>().AsImplementedInterfaces().SingleInstance();
             //builder.RegisterType<Mocks.MarketPlaceMock>().AsImplementedInterfaces().SingleInstance();
             builder.RegisterType<Repositories.MarketPlace>().AsImplementedInterfaces().SingleInstance();

        }
    }
}
