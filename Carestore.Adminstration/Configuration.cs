﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carestore.Adminstration
{
    public class Configuration : Interfaces.IConfiguration
    {
        public string CRIPAdress { get { return ConfigurationManager.AppSettings["CRIPAdress"]; } }
        public string MarketPlaceAdress { get { return ConfigurationManager.AppSettings["MarketPlaceAdress"]; } }
    }
}
