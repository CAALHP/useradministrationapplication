﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Carestore.Adminstration.Controls
{
    public class ExtendedDataTrigger : Microsoft.Expression.Interactivity.Core.DataTrigger
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            var element = this.AssociatedObject as FrameworkElement;
            if (element == null)
                return;
            element.Loaded += InvokeActionsOnLoad;
        }

        protected override void OnDetaching()
        {
            var element = this.AssociatedObject as FrameworkElement;
            if (element != null)
                element.Loaded -= this.InvokeActionsOnLoad;
            base.OnDetaching();
        }

        private void InvokeActionsOnLoad(object sender, RoutedEventArgs e)
        {
            this.EvaluateBindingChange(null);
        }
    }
}
