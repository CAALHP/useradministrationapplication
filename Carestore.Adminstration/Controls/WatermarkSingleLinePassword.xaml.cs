﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Carestore.Adminstration.Controls
{
    /// <summary>
    /// Interaction logic for WatermarkSingleLinePassword.xaml
    /// </summary>
    public partial class WatermarkSingleLinePassword : IDisposable
    {
 public static readonly DependencyProperty IsTextInvaildProperty = DependencyProperty.Register("IsTextInvaild", typeof(bool), typeof(WatermarkSingleLinePassword), new FrameworkPropertyMetadata(false));
        public static readonly DependencyProperty ShowPopupWhenWhenTextInvaildProperty = DependencyProperty.Register("ShowPopupWhenWhenTextInvaild", typeof(bool), typeof(WatermarkSingleLinePassword), new FrameworkPropertyMetadata(false));
        public static readonly DependencyProperty ErrorTextProperty = DependencyProperty.Register("ErrorText", typeof(string), typeof(WatermarkSingleLinePassword), new FrameworkPropertyMetadata(""));
        public static readonly DependencyProperty CharTextProperty = DependencyProperty.Register("CharText", typeof(string), typeof(WatermarkSingleLinePassword), new FrameworkPropertyMetadata(""));

        // .NET Property wrapper
        public bool IsTextInvaild
        {
            get { return (bool)GetValue(IsTextInvaildProperty); }
            set { SetValue(IsTextInvaildProperty, value); }
        }

        public bool ShowPopupWhenWhenTextInvaild
        {
            get { return (bool)GetValue(ShowPopupWhenWhenTextInvaildProperty); }
            set { SetValue(ShowPopupWhenWhenTextInvaildProperty, value); }
        }

        public string ErrorText
        {
            get { return (string)GetValue(ErrorTextProperty); }
            set { SetValue(ErrorTextProperty, value); }
        }

        public string CharText
        {
            get { return (string)GetValue(CharTextProperty); }
            private set { SetValue(CharTextProperty, value); }
        }

        public WatermarkSingleLinePassword()
        {
            InitializeComponent();
            this.Unloaded += (sender, args) => Dispose();
            this.TextChanged += OnTextChanged;
        }

        private void OnTextChanged(object sender, TextChangedEventArgs textChangedEventArgs)
        {
            var textLength = this.Text.Length;

            CharText = string.Empty;
            for (int i = 0; i < textLength; i++)
            {
                CharText += "*";
            }
        }

        private void OnLayoutUpdated(object sender, EventArgs eventArgs)
        {
            var popup = FindVisualChild<System.Windows.Controls.Primitives.Popup>(this);


            if (popup == null)
                return;

            if (popup.IsOpen)
            {
                var mi = typeof(System.Windows.Controls.Primitives.Popup).GetMethod("UpdatePosition", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                mi.Invoke(popup, null);
                popup.VerticalOffset = -(popup.Child.RenderSize.Height / 2);
            }
        }

        private childItem FindVisualChild<childItem>(DependencyObject obj) where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }

        public void Dispose()
        {
            var popup = FindVisualChild<System.Windows.Controls.Primitives.Popup>(this);

            if (popup == null)
                return;

            if (popup.IsOpen)
                popup.IsOpen = false;
        }

    }
}
