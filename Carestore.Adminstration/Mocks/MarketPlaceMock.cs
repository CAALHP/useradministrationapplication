﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Carestore.Adminstration.Interfaces;
using Carestore.Adminstration.Models;
using Newtonsoft.Json;
using RestSharp;

namespace Carestore.Adminstration.Mocks
{
    public class MarketPlaceMock : Interfaces.IMarketplace
    {
        private readonly Interfaces.IConfiguration _config;
        private readonly RestSharp.RestClient _restClient;
        private WebClient _webClient;

        public MarketPlaceMock(IConfiguration config)
        {
            if (config == null) throw new ArgumentNullException("config");
            _config = config;
            _restClient = new RestClient(_config.MarketPlaceAdress);
            _webClient = new WebClient();
        }

        public async Task<string> VerifyLoginAsync(UserCredentialsData userCredentials)
        {
            return "2";
        }

        public async Task<bool> UpdateUserAsync(User user)
        {
            await Task.Delay(2000);
            return true;
        }

        public async Task<IReadOnlyCollection<User>> GetUsersAsync(string managerId)
        {
            await Task.Delay(2000);
            var list = new List<User>();

            list.Add(new User { UserId = "1", Firstname = "Mads", Biometric = new List<string>()});
            list.Add(new User { UserId = "2", Firstname = "John", Biometric = new List<string>{"",""}});

            return list;
        }

        public async Task<User> GetUserAsync(string userId)
        {
            //var address = new Uri(_config.MarketPlaceAdress + "users/profile/" + "30d7a876-9bfc-49fc-83d3-1478b016f85a");
            //_webClient.Headers["Content-type"] = "application/json";

            //// invoke the REST method
            //var json = _webClient.DownloadString(address);

            //// deserialize from json
            //var profile = JsonConvert.DeserializeObject<User>(json);
            //return profile;
            return new User {UserId = "1", Firstname = "Mads", Biometric = new List<string>()};
        }
    }
}
