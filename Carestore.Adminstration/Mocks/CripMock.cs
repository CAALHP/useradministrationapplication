﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carestore.Adminstration.Mocks
{
    public class CripMock : Interfaces.ICRIP
    {
        public async Task<IReadOnlyCollection<string>> GetBiometrics()
        {
            await Task.Delay(2000);
            return new []{""};
        }

        public async Task<bool> StoreUserOnNFCCard(string uID)
        {
            await Task.Delay(2000);
            return true;
        }
    }
}
