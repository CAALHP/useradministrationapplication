﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Carestore.Adminstration.Interfaces;
using Carestore.Adminstration.Models;
using Newtonsoft.Json;
using RestSharp;

namespace Carestore.Adminstration.Repositories
{
    public class CRIP : Interfaces.ICRIP
    {
        private readonly Interfaces.IConfiguration _config;
        private readonly RestSharp.RestClient _restClient;

        public CRIP(IConfiguration config)
        {
            if (config == null) 
                throw new ArgumentNullException("config");

            _config = config;
            _restClient = new RestClient(_config.CRIPAdress);
        }

        public async Task<IReadOnlyCollection<string>> GetBiometrics()
        {
            try
            {
                var request = new RestRequest("biometric/templates", Method.GET);
                var response = await _restClient.ExecuteTaskAsync(request);

                var biometrics = JsonConvert.DeserializeObject<Repositories.Wrappers.BiometricsWrapper>(response.Content);
                return biometrics.templates;
            }
            catch (Exception e)
            {
                
            }

            return null;
        }

        public async Task<bool> StoreUserOnNFCCard(string uID)
        {
            try
            {
                var ser = ("{*uid*:*"+ uID +"*}").Replace('*', '"');

                var request = new RestRequest("/nfc/user", Method.POST);
                request.AddParameter("application/x-www-form-urlencoded", ser, ParameterType.RequestBody);
                var response = await _restClient.ExecuteTaskAsync(request);

                if (response.ResponseStatus != ResponseStatus.Completed)
                    return false;

                return true;
            }
            catch (Exception)
            {
                
            }

            return false;
        }
    }
}
