﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carestore.Adminstration.Repositories.Wrappers
{
    public class BiometricsWrapper
    {
        public List<string> templates { get; set; }
    }
}
