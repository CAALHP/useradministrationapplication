﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carestore.Adminstration.Repositories.Wrappers
{
    public class UserWrapper
    {
        public string FristName { get; set; }
        public string LastName { get; set; }
        public string ProfilePhotoURL { get; set; }
        public string UserId { get; set; }
        public int UserRole { get; set; }
        public List<string> Templates { get; set; }
        public string NFC { get; set; }
    }
}
