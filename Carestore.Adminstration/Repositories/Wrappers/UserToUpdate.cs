﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carestore.Adminstration.Repositories.Wrappers
{
    public class UserToUpdate
    {
        public string UserId { get; set; }

        public int UserRole { get; set; } // indicates type of the user

        public string[] Templates { get; set; }
    }
}
