﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Carestore.Adminstration.Interfaces;
using Carestore.Adminstration.Models;
using Carestore.Adminstration.Repositories.Wrappers;
using Newtonsoft.Json;
using RestSharp;
using ServiceStack.Common.Extensions;

namespace Carestore.Adminstration.Repositories
{
    public class MarketPlace : Interfaces.IMarketplace
    {
        private readonly Interfaces.IConfiguration _config;
        private readonly RestSharp.RestClient _restClient;

        public MarketPlace(IConfiguration config)
        {
            if (config == null)
                throw new ArgumentNullException("config");

            _config = config;
            _restClient = new RestClient(_config.MarketPlaceAdress);
            _restClient.CookieContainer = new CookieContainer();
        }

        public async Task<string> VerifyLoginAsync(UserCredentialsData userCredentials)
        {
            try
            {
                var request = new RestRequest("manager/authorize", Method.POST);
                request.AddJsonBody(userCredentials);
                var response = await _restClient.ExecuteTaskAsync(request);

                if (JsonConvert.DeserializeObject<string>(response.Content).ToLower() != "true")
                    return "0";
                
                request = new RestRequest("manager/getid", Method.GET);
                request.AddJsonBody(userCredentials);
                response = await _restClient.ExecuteTaskAsync(request);

                return JsonConvert.DeserializeObject<string>(response.Content);
            }
            catch (Exception)
            {
            }

            return "0";
        }

        public async Task<bool> UpdateUserAsync(User user)
        {
            try
            {
                var userToUpdate = new Wrappers.UserToUpdate { UserId = user.UserId, UserRole = user.Role, Templates = user.Biometric.ToArray() };

                var request = new RestRequest("users", Method.PUT);
                request.AddJsonBody(userToUpdate);
                var response = await _restClient.ExecuteTaskAsync(request);

                if (response.ResponseStatus != ResponseStatus.Completed)
                    return false;

                return true;
            }
            catch (Exception e)
            {
            }

            return false;
        }

        public async Task<IReadOnlyCollection<User>> GetUsersAsync(string managerId)
        {
            try
            {
                var request = new RestRequest("users/organization/" + managerId, Method.GET);
                var response = await _restClient.ExecuteTaskAsync(request);

                var users = JsonConvert.DeserializeObject<IReadOnlyCollection<UserWrapper>>(response.Content);
                var resultList = users.Select(u => new User {UserId = u.UserId, Firstname = u.FristName, Lastname = u.LastName, Biometric = u.Templates, Role = u.UserRole, PictureURL = u.ProfilePhotoURL, NFC = u.NFC}).ToList();

                return resultList.OrderBy(x => x.Firstname).ToList();
            }
            catch (Exception)
            {
            }

            return null;
        }
    }
}