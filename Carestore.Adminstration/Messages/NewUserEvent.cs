﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carestore.Adminstration.Messages
{
    public class NewUserEvent
    {
        public Models.User User { get; set; }
    }
}
